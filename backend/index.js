const express = require('express');
const WebSocket = require('ws');
const http = require('http');
const cors = require('cors')

const app = express();
app.use(express.json())

app.use(cors())
const port = 4000;

let messages = [];

app.post('/api/message', (req, res) => {
    
    const { message } = req.body;
    const messageData = { id: Date.now(), message };
    messages.push(messageData);
    broadcastMessages();
    res.status(201).send(messageData);
  });
  
  
  app.delete('/api/message/:id', (req, res) => {
    const { id } = req.params;
    messages = messages.filter(message => message.id !== Number(id));
    broadcastMessages();
    res.status(204).end();
  });
  
  
  app.get('/api/messages', (req, res) => {
    res.status(200).json(messages);
  });
    
  const server = http.createServer(app); 
  const wss = new WebSocket.Server({ server }); 

  wss.on('connection', ws => {
    console.log('Client connected');
  
    ws.on('message', message => {
      console.log('Received:', message);
    });
  
    ws.on('close', () => {
      console.log('Client disconnected');
    });
  });
  
  function broadcastMessages() {
    const payload = JSON.stringify({
        type: 'UPDATE_MESSAGES',
        messages,
    });
    wss.clients.forEach(client => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(payload);
      }
    });
  }
  
 
  server.listen(port, () => {
    console.log(`Server listening at http://localhost:${port}`);
  });
