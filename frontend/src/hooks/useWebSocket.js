import { useEffect } from 'react';
import { useQueryClient } from 'react-query';

export const useWebSocket = () => {
  const queryClient = useQueryClient();

  useEffect(() => {
    const ws = new WebSocket('ws://localhost:4000/');

    ws.onmessage = (event) => {
      const { type, messages } = JSON.parse(event.data);

      if (type === 'UPDATE_MESSAGES') {
        queryClient.setQueryData('messages', messages);
      }
    };

    return () => {
      if (ws.readyState === 1) {
        ws.close();
      }
    };
  }, [queryClient]);
};
