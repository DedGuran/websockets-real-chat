export const fetchMessages = async () => {
  try {
    const response = await fetch('http://localhost:4000/api/messages');
    if (!response.ok) {
      console.error('Error for Get All messages');
    }
    console.log('test', response);
    return response.json();
  } catch (error) {
    console.error('Error fetchMessages:', error);
    return null;
  }
};

export const sendMessage = async (message) => {
  try {
    const response = await fetch('http://localhost:4000/api/message', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ message })
    });
    if (!response.ok) {
      console.error('Error Post Message');
    }
    return response.json();
  } catch (error) {
    console.error('Error sendMessage:', error);
    return null;
  }
};

export const deleteMessage = async (id) => {
  try {
    const response = await fetch(`http://localhost:4000/api/message/${id}`, {
      method: 'DELETE'
    });

    if (response.ok) {
      console.log('Message deleted successfully');
    } else {
      console.error('Failed to delete the message');
    }
  } catch (error) {
    console.error('Error deleteMessage:', error);
  }
};
