import {useState} from 'react'
import { QueryClient, QueryClientProvider } from 'react-query';
import MessageList from './components/MessageList';
import AddMessageForm from './components/AddMessageForm';
import './App.css';

const queryClient = new QueryClient();

function App() {
  const [messages, setMessages] = useState([]);

  const handleSendMessage = (newMessage) => {
    setMessages([...messages, newMessage]);
  };
  return (
    <QueryClientProvider client={queryClient}>
    <div className="App p-4">
      <div className="max-w-md mx-auto">
        <div className="mb-4 text-2xl font-semibold text-center">Простой чат</div>
        <MessageList messages={messages} />
        <AddMessageForm onSendMessage={handleSendMessage} messages={messages} setMessages={setMessages}/>
      </div>
    </div>
  </QueryClientProvider>
  );
}

export default App;
