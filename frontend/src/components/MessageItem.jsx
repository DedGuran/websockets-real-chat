import React from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { deleteMessage } from '../api/api';

const MessageItem = ({ message, id }) => {
  const queryClient = useQueryClient();
  const { mutate } = useMutation(deleteMessage, {
    onSuccess: () => {
      queryClient.invalidateQueries('messages');
    }
  });
  const handleDelete = () => {
    mutate(id);
  };
  return (
    <div className="flex gap-x-4 items-center">
      <div className="w-[50px] h-[50px] rounded-full bg-blue-200"></div>
      <div className="p-2 bg-white rounded shadow p-4 relative">
        {message}
        <button
          className="w-[10px] h-[10px] text-red-500 absolute top-0 right-0"
          onClick={handleDelete}
        >
          X
        </button>
      </div>
    </div>
  );
};

export default MessageItem;
