import MessageItem from './MessageItem';
import { useQuery } from 'react-query';
import { fetchMessages } from '../api/api';
import { useWebSocket } from '../hooks/useWebSocket';

function MessageList() {
  useWebSocket();
  const {
    data: messages,
    isLoading,
    error
  } = useQuery('messages', fetchMessages, {
    staleTime: 5000,
    refetchOnWindowFocus: false
  });

  if (isLoading) return <div>Загрузка...</div>;
  if (error) return <div>Ошибка при загрузке: {error.message}</div>;
  return (
    <div className="overflow-auto h-96 p-4 space-y-2 bg-gray-100 rounded-lg">
      {messages.map((messageObj) => (
        <div key={messageObj.id}>
          <MessageItem message={messageObj.message} id={messageObj.id} />
        </div>
      ))}
    </div>
  );
}
export default MessageList;
