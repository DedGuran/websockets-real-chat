import { useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { sendMessage } from '../api/api';

const AddMessageForm = ({ onSendMessage }) => {
  const [message, setMessage] = useState();
  const queryClient = useQueryClient();

  const mutation = useMutation(sendMessage, {
    onSuccess: () => {
      onSendMessage(message);
      queryClient.invalidateQueries('messages');
      setMessage('');
    }
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    mutation.mutate(message);
    //onSendMessage(message);
    //setMessage('');
  };
  return (
    <form onSubmit={handleSubmit} className="mt-4 flex">
      <input
        type="text"
        value={message}
        onChange={(e) => setMessage(e.target.value)}
        placeholder="Введите ваше сообщение здесь..."
        className="w-full p-2 border rounded-l-lg focus:outline-none focus:ring-2"
      />
      <button
        type="submit"
        className="bg-blue-500 text-white p-2 rounded-r-lg hover:bg-blue-600"
      >
        Отправить
      </button>
    </form>
  );
};

export default AddMessageForm;
